const env = 'production';

const hosts = {
  development: '',
  production: 'http://47.106.35.84/marriage/index.php?'
}

const api = {
  user: {
    detail: {
      method: 'POST',
      url: '/person/getPersonalMsg'
    },
    wechatInfo: {
      method: 'POST',
      url: '/person/verificationWechatLogin'
    },
    regist: {
      method: 'POST',
      url: '/person/userSignin'
    },
    login: {
      method: 'POST',
      url: '/person/authenticate'
    },
    change: {
      method: 'POST',
      url: '/common/modifyPassword'
    },
    checkLogin: {
      method: 'POST',
      url: '/person/getIfUserSigned'
    }
  },
  rec: {
    list: {
      method: 'POST',
      url: '/person/getRecommendMsg'
    },
    nlist: {
      method: 'POST',
      url: '/common/getMainList'
    }
  }
}

module.exports = {
  env,
  api: disposeUrl(api, hosts[env])
}

function disposeUrl(obj, prefix) {
  Object.keys(obj).forEach(v => {
    if (obj[v].url) {
      obj[v].url = prefix + obj[v].url
    } else {
      obj[v] = disposeUrl(obj[v], prefix)
    }
  })
  return obj
}