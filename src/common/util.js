import wepy from 'wepy'

export const requestIntercept = {
  config(p) {
    wepy.showLoading({
      title: '加载中',
      mask: true,
    })
    return p
  },
  success (p) {
    wepy.hideLoading()
    return p
  },
  fail (p) {
    wepy.hideLoading()
    showErrorMsg('请求失败，请稍后重试')
    return p
  }
}
export function showErrorMsg(msg, title='提醒') {
  wepy.showModal({
    title: title,
    content: msg,
    showCancel: false,
    confirmColor: '#362f7f'
  })
}
export function showSuccessMsg(msg, title='提示') {
  wepy.showModal({
    title: title,
    content: msg,
    showCancel: false
  })
}
export function requestData(options, success, error) {
  wepy.request(options).then(data => {
    if (data.statusCode == 200) {
      success(data.data)
    } else {
      showErrorMsg('http request error:' + data.statusCode)
      error(data)
    }
  })
}
export function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}
export function formatDate(date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('-')
}
export function formatTimestamp(date) {
  return +new Date(date)
}