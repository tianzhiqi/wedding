import wepy from 'wepy'
import { api } from './config'
import { requestData } from './util'

export const fetchUserDetail = (params) => {
  return new Promise((resolve, reject) => {
    requestData({
      url: api.user.detail.url,
      method: api.user.detail.method,
      header: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        token: wepy.getStorageSync('LOGIN')
      },
      data: params
    }, function(data) {
      resolve(data)
    }, function(error) {
      reject(error)
    })
  })
}
/**
 * 推荐
 */
export const fetchRecList = (params) => {
  return new Promise((resolve, reject) => {
    requestData({
      url: api.rec.list.url,
      method: api.rec.list.method,
      header: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        token: wepy.getStorageSync('LOGIN')
      },
      data: params
    }, function(data) {
      resolve(data)
    }, function(error) {
      reject(error)
    })
  })
}
/**
 * 登录后推荐
 */
export const fetchList = (params) => {
  return new Promise((resolve, reject) => {
    requestData({
      url: api.rec.nlist.url,
      method: api.rec.nlist.method,
      header: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        token: wepy.getStorageSync('LOGIN')
      },
      data: params
    }, function(data) {
      resolve(data)
    }, function(error) {
      reject(error)
    })
  })
}
/**
 * 注册
 */
export const registUser = (params) => {
  return new Promise((resolve, reject) => {
    requestData({
      url: api.user.regist.url,
      method: api.user.regist.method,
      header: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        token: wepy.getStorageSync('LOGIN')
      },
      data: params
    }, function(data) {
      resolve(data)
    }, function(error) {
      reject(error)
    })
  })
}
/**
 * 修改密码
 */
export const changeUser = (params) => {
  return new Promise((resolve, reject) => {
    requestData({
      url: api.user.change.url,
      method: api.user.change.method,
      header: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        token: wepy.getStorageSync('LOGIN')
      },
      data: params
    }, function(data) {
      resolve(data)
    }, function(error) {
      reject(error)
    })
  })
}
/**
 * 登录
 */
export const login = (params) => {
  return new Promise((resolve, reject) => {
    requestData({
      url: api.user.login.url,
      method: api.user.login.method,
      header: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        token: wepy.getStorageSync('LOGIN')
      },
      data: params
    }, function(data) {
      resolve(data)
    }, function(error) {
      reject(error)
    })
  })
}
/**
 * 检测登录
 */
export const checkLogin = (params) => {
  return new Promise((resolve, reject) => {
    requestData({
      url: api.user.checkLogin.url,
      method: api.user.checkLogin.method,
      header: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        token: wepy.getStorageSync('LOGIN')
      },
      data: params
    }, function(data) {
      resolve(data)
    }, function(error) {
      reject(error)
    })
  })
}